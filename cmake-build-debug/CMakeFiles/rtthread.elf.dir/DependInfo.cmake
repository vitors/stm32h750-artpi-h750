
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "ASM"
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_ASM
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/CMSIS/Device/ST/STM32H7xx/Source/Templates/gcc/startup_stm32h750xx.s" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/CMSIS/Device/ST/STM32H7xx/Source/Templates/gcc/startup_stm32h750xx.s.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/libcpu/arm/cortex-m7/context_gcc.S" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/libcpu/arm/cortex-m7/context_gcc.S.obj"
  )
set(CMAKE_ASM_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_ASM
  "HAVE_CCONFIG_H"
  "RT_USING_NEWLIB"
  "STM32H750xx"
  "USE_HAL_DRIVER"
  "__RTTHREAD__"
  )

# The include file search paths:
set(CMAKE_ASM_TARGET_INCLUDE_PATH
  "../"
  "../board"
  "../board/CubeMX_Config/Core/Inc"
  "../board/port"
  "../libraries/HAL_Drivers"
  "../libraries/HAL_Drivers/config"
  "../libraries/STM32H7xx_HAL/CMSIS/Device/ST/STM32H7xx/Include"
  "../libraries/STM32H7xx_HAL/CMSIS/Include"
  "../libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Inc"
  "../rt-thread/components/drivers/include"
  "../rt-thread/components/finsh"
  "../rt-thread/components/libc/compilers/common"
  "../rt-thread/components/libc/compilers/gcc/newlib"
  "../rt-thread/include"
  "../rt-thread/libcpu/arm/common"
  "../rt-thread/libcpu/arm/cortex-m7"
  )
set(CMAKE_DEPENDS_CHECK_C
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/applications/main.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/applications/main.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/board/CubeMX_Config/Core/Src/stm32h7xx_hal_msp.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/board/CubeMX_Config/Core/Src/stm32h7xx_hal_msp.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/board/board.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/board/board.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/HAL_Drivers/drv_common.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/HAL_Drivers/drv_common.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/HAL_Drivers/drv_gpio.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/HAL_Drivers/drv_gpio.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/HAL_Drivers/drv_usart.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/HAL_Drivers/drv_usart.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/CMSIS/Device/ST/STM32H7xx/Source/Templates/system_stm32h7xx.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/CMSIS/Device/ST/STM32H7xx/Source/Templates/system_stm32h7xx.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_cec.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_cec.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_comp.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_comp.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_cortex.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_cortex.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_crc.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_crc.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_crc_ex.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_crc_ex.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_cryp.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_cryp.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_cryp_ex.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_cryp_ex.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_dma.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_dma.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_dma_ex.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_dma_ex.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_gpio.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_gpio.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_mdma.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_mdma.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_pwr.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_pwr.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_pwr_ex.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_pwr_ex.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_rcc.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_rcc.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_rcc_ex.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_rcc_ex.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_rng.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_rng.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_sram.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_sram.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_uart.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_uart.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_uart_ex.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_uart_ex.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_usart.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_usart.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/components/drivers/misc/pin.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/components/drivers/misc/pin.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/components/drivers/serial/serial.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/components/drivers/serial/serial.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/components/drivers/src/completion.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/components/drivers/src/completion.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/components/drivers/src/dataqueue.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/components/drivers/src/dataqueue.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/components/drivers/src/pipe.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/components/drivers/src/pipe.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/components/drivers/src/ringblk_buf.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/components/drivers/src/ringblk_buf.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/components/drivers/src/ringbuffer.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/components/drivers/src/ringbuffer.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/components/drivers/src/waitqueue.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/components/drivers/src/waitqueue.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/components/drivers/src/workqueue.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/components/drivers/src/workqueue.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/components/finsh/cmd.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/components/finsh/cmd.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/components/finsh/msh.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/components/finsh/msh.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/components/finsh/shell.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/components/finsh/shell.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/components/libc/compilers/common/time.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/components/libc/compilers/common/time.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/components/libc/compilers/gcc/newlib/syscalls.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/components/libc/compilers/gcc/newlib/syscalls.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/libcpu/arm/common/backtrace.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/libcpu/arm/common/backtrace.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/libcpu/arm/common/div0.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/libcpu/arm/common/div0.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/libcpu/arm/common/showmem.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/libcpu/arm/common/showmem.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/libcpu/arm/cortex-m7/cpu_cache.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/libcpu/arm/cortex-m7/cpu_cache.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/libcpu/arm/cortex-m7/cpuport.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/libcpu/arm/cortex-m7/cpuport.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/src/clock.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/src/clock.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/src/components.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/src/components.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/src/device.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/src/device.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/src/idle.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/src/idle.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/src/ipc.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/src/ipc.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/src/irq.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/src/irq.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/src/kservice.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/src/kservice.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/src/mem.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/src/mem.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/src/mempool.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/src/mempool.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/src/object.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/src/object.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/src/scheduler.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/src/scheduler.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/src/thread.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/src/thread.c.obj"
  "/Users/chenhonglin/dist/stm32h750-artpi-h750/rt-thread/src/timer.c" "/Users/chenhonglin/dist/stm32h750-artpi-h750/cmake-build-debug/CMakeFiles/rtthread.elf.dir/rt-thread/src/timer.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "HAVE_CCONFIG_H"
  "RT_USING_NEWLIB"
  "STM32H750xx"
  "USE_HAL_DRIVER"
  "__RTTHREAD__"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../"
  "../board"
  "../board/CubeMX_Config/Core/Inc"
  "../board/port"
  "../libraries/HAL_Drivers"
  "../libraries/HAL_Drivers/config"
  "../libraries/STM32H7xx_HAL/CMSIS/Device/ST/STM32H7xx/Include"
  "../libraries/STM32H7xx_HAL/CMSIS/Include"
  "../libraries/STM32H7xx_HAL/STM32H7xx_HAL_Driver/Inc"
  "../rt-thread/components/drivers/include"
  "../rt-thread/components/finsh"
  "../rt-thread/components/libc/compilers/common"
  "../rt-thread/components/libc/compilers/gcc/newlib"
  "../rt-thread/include"
  "../rt-thread/libcpu/arm/common"
  "../rt-thread/libcpu/arm/cortex-m7"
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
